package br.com.rbeninca.listafrutas;

import android.content.Context;
import android.media.Image;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

class FrutaAdapterRecyclerView extends RecyclerView.Adapter<FrutaAdapterRecyclerView.MyViewHolder> {

    Context mContext;
    int mResource;
    Fruta[] mDataset;

    public FrutaAdapterRecyclerView(Context mContext, int mResource, Fruta[]<Fruta>mDataset Fruta[] FRUTAS) {
        this.mContext = mContext;
        this.mResource = mResource;
        this.mDataset = mDataset;
    }


    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(mContext);
        View convertView = layoutInflater.inflate(mResource, parent, false);
        MyViewHolder myViewHolder = new MyViewHolder(convertView);
        return null;
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {

    }

    @Override
    public int getItemCount() {
        return mDataset.length;
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        TextView tvCodigo;
        TextView tvNome;
        TextView tvPreco;
        TextView tvPrecoVenda;
        ImageView imgView;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);

             tvCodigo = (TextView) itemView.findViewById(R.id.tvCodigo);
             tvNome = (TextView) itemView.findViewById(R.id.tvCodigo);
             tvPreco = (TextView) itemView.findViewById(R.id.tvCodigo);
             tvPrecoVenda = (TextView) itemView.findViewById(R.id.tvCodigo);
             imgView = (ImageView) itemView.findViewById(R.id.imageView);


        }
    }

    }

